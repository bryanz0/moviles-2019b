fun main() {
    User.printName()
    User.name = "Peter"
    User.printName()
}

object User{
    var name = "John"

    init {
        println("Singleton invoked")
    }

    fun printName() = println(name)
}